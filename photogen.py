"""
Tool for generating an picture overview page from photos and metadata.
"""

import os
import sys
import errno
import datetime
import yaml
import jinja2
from PIL import Image

IMAGE_SUFFIXES = [".jpg", ".JPG"]
THUMBNAIL_SIZE = (300, 300)

EXIF_DATE_ORIGINAL = 36867
EXIF_DATE_DIGITIZED = 36868

def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

def creation_date(path):
    """
    Return creation date of photo given by ``path``.
    """
    exif = Image.open(path)._getexif()
    for tag_id in [EXIF_DATE_ORIGINAL, EXIF_DATE_DIGITIZED]:
        try:
            return datetime.datetime.strptime(exif[tag_id], "%Y:%m:%d %H:%M:%S")
        except KeyError:
            pass
    filebase = os.path.splitext(os.path.split(path)[-1])[0]
    try:
        return datetime.datetime.strptime(filebase, "%Y%m%d_%H%M%S")
    except ValueError:
        pass
    t = os.path.getmtime(path)
    return datetime.datetime.fromtimestamp(t)

class ImageDescriptor(object):
    def __init__(self, image_path, metadata=None):
        self.image_path = image_path
        if metadata is None:
            self.metadata = {}
        else:
            self.metadata = metadata
        self._outfolder = os.getcwd()
    def set_output_folder(self, outfolder):
        self._outfolder = outfolder
    @property
    def image_url(self):
        return os.path.relpath(self.image_path, self._outfolder)
    @property
    def thumbnail_path(self):
        return os.path.join(self._outfolder, "thumbnails", os.path.split(self.image_path)[-1])
    @property
    def thumbnail_url(self):
        return os.path.relpath(self.thumbnail_path, self._outfolder)
    @property
    def detail_path(self):
        return os.path.join(self._outfolder, os.path.split(self.image_path)[-1] + ".html")
    @property
    def detail_url(self):
        return os.path.relpath(self.detail_path, self._outfolder)
    @property
    def short_description(self):
        return self.metadata.get("short", "-")
    @property
    def long_description(self):
        return self.metadata.get("description", "")
    @property
    def description(self):
        return self.short_description
    @property
    def people(self):
        print(self.metadata["people"])
        return self.metadata["people"]
    @property
    def creator(self):
        return self.metadata["creator"]
    def __contains__(self, key):
        print(key, self.metadata.keys())
        return key in self.metadata

def find_images(image_folder, metadata_folder=None):
    candidates = [candidate
                  for candidate in os.listdir(image_folder)
                  if os.path.splitext(candidate)[1] in IMAGE_SUFFIXES]
    for candidate in sorted(candidates, key=lambda name: creation_date(os.path.join(image_folder, name))):
        metadata_candidate = os.path.join(metadata_folder,
                                          "{}.yaml".format(candidate))
        if os.path.exists(metadata_candidate):
            with open(metadata_candidate, encoding="utf-8") as metadata_file:
                metadata = yaml.load(metadata_file)
        else:
            metadata = None
        yield ImageDescriptor(os.path.join(image_folder, candidate),
                              metadata)

def render_site(images, output_folder, template_folder=None):
    if template_folder is None:
        basepath = os.path.abspath(os.path.dirname(__file__))
        template_folder = os.path.join(basepath, "templates")
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_folder))
    def render_page(outfile, template_name, **kwargs):
        template = env.get_template(template_name)
        page = template.render(**kwargs)
        with open(outfile, "w", encoding="utf-8") as outfile:
            outfile.write(page)
    for image in images:
        image.set_output_folder(output_folder)
        make_sure_path_exists(os.path.dirname(image.thumbnail_path))
        with open(image.image_path, "rb") as imgfile:
            img = Image.open(imgfile)
            img.thumbnail(THUMBNAIL_SIZE)
            img.save(image.thumbnail_path)
        render_page(image.detail_path,
                    "picture.html",
                    title=image.short_description,
                    image=image)

    render_page(os.path.join(output_folder, "index.html"),
                "index.html",
                title="Pictures",
                images=images)
    render_page(os.path.join(output_folder, "stylesheet.css"), "stylesheet.css")


def _main():
    image_folder = os.path.abspath(sys.argv[1])
    if len(sys.argv) > 2:
        metadata_folder = os.path.abspath(sys.argv[2])
    else:
        metadata_folder = None
    images = list(find_images(image_folder, metadata_folder))
    for image in images:
        print(image.metadata)
    render_site(images, "output")

if __name__ == '__main__':
    _main()
