# photogen -- photo index generator
Usage: call photogen like:

    python3 photogen.py <picture-folder> <metadata-folder>

photogen will create an ``output``-folder in the current directory, containing several html files.
Metadata must be given as one YAML file per photo. The name must be ``<photo-filename>.yaml`` including the suffix of the photo.

An exemplary yaml file might look like:

    ---
    creator: Tobias Kölling
    license:
    short: "Ground Work: need more electricity"
    description: "some more explanatory text"
    people:
        order: center
        names:
            - Kevin Wolf

Valid ``order`` parameters are:
- ``left to right``
- ``center``
